import socket
import sys
import os.path

# check all parameters given
if len(sys.argv) != 4:
    print("parameters: filename ip-address port")
    exit(0)

filename = sys.argv[1]
address = sys.argv[2]
port = int(sys.argv[3])

# check file exists
if not os.path.isfile(filename):
    print("File not found")
    exit(0)

with open(filename, "rb") as f:
    file_contents = f.read()

# send file
conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect((address, port))
conn.sendall(filename.encode() + b"<<FILENAME>>" + file_contents + b"<<TERMINATOR>>")

# show how it was saved
saved = conn.recv(32).decode()
print(f"Saved on server as {saved}")
