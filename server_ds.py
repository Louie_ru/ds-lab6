import socket
import threading
import os.path


PORT = 30800
BUFFER_SIZE = 2048


def receive_all(sock) -> bytes:
    # receive until trailer comes
    data = b''
    while b"<<TERMINATOR>>" not in data:
        data += sock.recv(BUFFER_SIZE)
    return data.split(b"<<TERMINATOR>>")[0]  # remove trailer


def append_number(filename: str, num: int):
    # calculate next file name in case of collision
    if '.' in filename:
        filename = "".join(filename.split('.')[:-1]) + str(num) + "." + filename.split('.')[-1]
    else:
        filename = filename + str(num)
    return filename


def receive_file(conn, addr: tuple) -> None:
    # data came from client
    received = receive_all(conn)
    filename, file_contents = received.split(b"<<FILENAME>>")
    filename = filename.decode()

    # append number to avoid collisions
    if os.path.isfile(filename):
        i = 1
        while os.path.isfile(append_number(filename, i)):
            i += 1
        filename = append_number(filename, i)

    print(f"received file {filename} with size {len(file_contents)} from {addr}")
    with open(filename, "wb") as f:
        f.write(file_contents)
    conn.send(filename.encode())
    conn.close()


tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcp_server.bind(('', PORT))
tcp_server.listen(5)

while True:
    # always listen and process file in new thread
    conn, addr = tcp_server.accept()
    threading.Thread(target=receive_file, args=(conn, addr), daemon=True).start()
